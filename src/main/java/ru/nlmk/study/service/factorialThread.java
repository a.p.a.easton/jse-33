package ru.nlmk.study.service;

import java.math.BigInteger;
import java.util.concurrent.Callable;

public class factorialThread implements Callable<BigInteger> {
    private Integer start;
    private Integer end;

    public factorialThread(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public BigInteger call() {
        BigInteger result = BigInteger.valueOf(start);
        for(int i = start; i < end; i++) {
            result = result.multiply(BigInteger.valueOf(i+1));
        }
        return result;
    }
}
